﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class playerLife : MonoBehaviour{
    
    public float barSpeed = 1f;
    public Image healtBar;
    private float nextHealth;

    private void Awake(){
        healtBar.fillAmount = 0f;    
        nextHealth = healtBar.fillAmount;
    }
    private void Update(){
        if (healtBar.fillAmount != nextHealth){
            healtBar.fillAmount = Mathf.MoveTowards(healtBar.fillAmount, nextHealth, Time.deltaTime * barSpeed);
        }   
    }
    public void SetHealth(float healthPercentage){
        nextHealth = healthPercentage;
    }
}