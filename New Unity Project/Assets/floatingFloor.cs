﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class floatingFloor : MonoBehaviour
{
    Animator animator;
    Rigidbody2D rb;
    public float walkvel=2;
    float limitLeft;
    float limitRigth;
    int direction=1;

    float velocity;
    Vector3 orginalscale;
   

    // Start is called before the first frame update
    void Start()
    {
        animator=GetComponent<Animator>();
        rb=GetComponent<Rigidbody2D>();
        limitLeft = transform.position.x - GetComponent<CircleCollider2D>().radius;
        limitRigth = transform.position.x + GetComponent<CircleCollider2D>().radius;
        orginalscale=transform.localScale;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
       rb.velocity=new Vector2(walkvel*direction,rb.velocity.y);
        
        if (transform.position.x<limitLeft) direction=1;
        if (transform.position.x>limitRigth) direction=-1;
        transform.localScale=new Vector3(orginalscale.x*direction,orginalscale.y,orginalscale.z);
    }
   
}

