﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class TIMER : MonoBehaviour
{
    public Text timer;
    public float tiempo = 0.0f;
    
    public void Update()
    {
        tiempo+= Time.deltaTime;
        timer.text=""+tiempo.ToString("f0");
        PlayerPrefs.SetString("tiempo", ""+tiempo.ToString("f0"));
        
    }
}
