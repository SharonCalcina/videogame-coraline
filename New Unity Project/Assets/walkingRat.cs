﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class walkingRat : MonoBehaviour
{
    public Animator animator;
    public Rigidbody2D rb;
    public float walkvel=2;
    float limitLeft;
    float limitRigth;
    int direction=1;
    bool dead=false;
    float velocity;
    Vector3 orginalscale;
    bool m=false;

    // Start is called before the first frame update
    void Start()
    {
        //animator=GetComponent<Animator>();
        //rb=GetComponent<Rigidbody2D>();
        limitLeft = transform.position.x - GetComponent<CircleCollider2D>().radius;
        limitRigth = transform.position.x + GetComponent<CircleCollider2D>().radius;
        orginalscale=transform.localScale;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        rb.velocity=new Vector2(walkvel*direction,rb.velocity.y);
        
        if (transform.position.x<limitLeft) direction=1;
        if (transform.position.x>limitRigth) direction=-1;
        transform.localScale=new Vector3(orginalscale.x*direction,orginalscale.y,orginalscale.z);
        if(animator.GetBool("dead")) {
            StartCoroutine("wait");
        }
    }
    IEnumerator wait(){
        yield return new WaitForSeconds(0.4f);
        Destroy(gameObject);
    }

    public void OnTriggerEnter2D(Collider2D col){
        float yoffsetR=1.516f;
        float yoffsetW=1.45f;
        if(col.gameObject.tag=="Player"){
            if(transform.position.y+yoffsetR<col.transform.position.y||
            transform.position.y+yoffsetW<col.transform.position.y){
                Debug.Log("player detentedddd");
                dead=true;
                animator.SetBool("dead",dead);
                 
            }
        }
        
    }
}
