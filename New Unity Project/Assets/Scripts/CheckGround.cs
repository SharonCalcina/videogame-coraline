﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckGround : MonoBehaviour
{
     public static bool isGrounded;

  
    void OnCollisionEnter(Collision other){
        if(other.gameObject.tag == "ground"){
            isGrounded = true;
        }
    }
    
    void OnCollisionExit(Collision other){
        if(other.gameObject.tag == "ground"){
            isGrounded = false;
        }
    }

}
