﻿
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Debug = UnityEngine.Debug;
public class PlayerControl : MonoBehaviour {
	public playerLife playerStatusUI;
    public int maxHealth { get; private set; }
    public int currentHealth { get; private set; }
    public float healthRange { get { return (float)currentHealth / (float)maxHealth; } }

	public playerLife blueStatusUI;
    public int maxBlue { get; private set; }
    public int currentBlue { get; private set; }
    public float blueRange { get { return (float)currentBlue / (float)maxBlue; } }

	public playerLife GreenStatusUI;
    public int maxGreen { get; private set; }
    public int currentGreen { get; private set; }
    public float greenRange { get { return (float)currentGreen / (float)maxGreen; } }

	public playerLife VioletStatusUI;
    public int maxViolet { get; private set; }
    public int currentViolet { get; private set; }
    public float violetRange { get { return (float)currentViolet / (float)maxViolet; } }


  public GameObject verde1;
  public GameObject verde2;
  public GameObject verde3;
  public GameObject verde4;
  public GameObject verde5;
  bool Verde1=false;
  bool Verde2=false;
  bool Verde3=false;
  bool Verde4=false;
  bool Verde5=false;

  public GameObject violeta1;
  public GameObject violeta2;
  public GameObject violeta3;
  public GameObject violeta4;
  public GameObject violeta5;
  bool Violeta1=false;
  bool Violeta2=false;
  bool Violeta3=false;
  bool Violeta4=false;
  bool Violeta5=false;

  public GameObject azul1;
  public GameObject azul2;
  public GameObject azul3;
  public GameObject azul4;
  public GameObject azul5;
  bool AzulTouch=false;
  bool Azul1=false;
  bool Azul2=false;
  bool Azul3=false;
  bool Azul4=false;
  bool Azul5=false;

  public GameObject ROJO;
  public GameObject DORADO;
  public GameObject LILA;
  int Az=0;
  int Ver=0;
  int Vio=0;
  public GameObject panelr;
  public GameObject paneld;
  public GameObject panell;
  public GameObject panelGana;
  public GameObject panelPierde;

  public GameObject jumpAudio;
  public GameObject walkAudio;
  public float runSpeed=2;
  public float jumpSpeed=3;
  public Animator anim;
  public Rigidbody2D rb2D;
  public bool enpiso;
  public bool enpiso2;
  public Transform refpie;
  public float radio;
  public float velX = 10f;
  int completo=0;
  bool compleAz=false;
  bool compleVer=false;
  bool compleVio=false;
  bool puerta = false;
  int muerte=0;
  //int[] arrayInts=new int[10];
  //int[] arrayInts1=new int[10];
  //int[] arrayInts2=new int[10];

  void Start(){
		maxHealth = 100;
		currentHealth = maxHealth;

		maxBlue = 10;
		currentBlue = maxBlue;

		maxGreen = 10;
		currentGreen = maxGreen;

		maxViolet = 10;
		currentViolet = maxViolet;

		//anim=GetComponent<Animator>();
		//rb2D=GetComponent<Rigidbody2D>();
  }
  public void OnTriggerEnter2D(Collider2D col){
        float xoffsetR=1f;
		float xoffsetR2=1.45f;
        if(col.gameObject.tag=="Rat"){
            if(transform.position.x<col.transform.position.x+xoffsetR||transform.position.x<col.transform.position.x+xoffsetR2){
                TakeDamage(5);
                Debug.Log("health"+currentHealth);
                muerte+=5;
                Debug.Log("muerte"+muerte);
                if(currentHealth==0){
                    Time.timeScale=0;  
                    panelPierde.gameObject.SetActive(true);
                }        
            }
        }
        if(col.gameObject.tag=="RatRed"){
            if(transform.position.x<col.transform.position.x+xoffsetR||transform.position.x<col.transform.position.x+xoffsetR2){
                if(compleVer==false && Ver>=1){
                    Ver=Ver-1;
                    EmptyVerde(2);
                    verde1.gameObject.SetActive(true);
                    verde2.gameObject.SetActive(true); 
                    verde3.gameObject.SetActive(true);
                    verde4.gameObject.SetActive(true); 
                    verde5.gameObject.SetActive(true);
                }
                if(compleVio==false && Vio>=1){
                    Vio--;
                    EmptyVioleta(2);
                    violeta1.gameObject.SetActive(true);
                    violeta2.gameObject.SetActive(true);
                    violeta3.gameObject.SetActive(true);
                    violeta4.gameObject.SetActive(true);
                    violeta5.gameObject.SetActive(true);
                }       
                if(compleAz==false && Az>=1){
                    Az=Az-1;
                    EmptyAzul(2);
                    azul1.gameObject.SetActive(true);
                    azul2.gameObject.SetActive(true);
                    azul3.gameObject.SetActive(true);
                    azul4.gameObject.SetActive(true);
                    azul5.gameObject.SetActive(true);
                }      
            }  
        }



                /*if(Verde1||Verde2||Verde3||Verde4||Verde5){
                    int posicionElim=Random.Range(1,5);

                    while(arrayInts1[posicionElim]!=1){
                        posicionElim=Random.Range(1,5);
                    }
                    if(posicionElim==1){
                        arrayInts1[1]=0;
                        EmptyVerde(1);
                        verde1.gameObject.SetActive(true);
                    }  
                    if(posicionElim==2){
                        arrayInts1[2]=0;
                        EmptyVerde(1);
                        verde2.gameObject.SetActive(true);  
                    }  
                    if(posicionElim==3){
                        arrayInts1[3]=0;
                        EmptyVerde(1);
                        verde3.gameObject.SetActive(true);
                    }  
                    if(posicionElim==4){
                        arrayInts1[4]=0;
                        EmptyVerde(1);
                        verde4.gameObject.SetActive(true);
                    }  
                    if(posicionElim==5){
                        arrayInts1[5]=0;
                        EmptyVerde(1);
                        verde5.gameObject.SetActive(true);
                    }  
                }      
                
                if(Violeta1||Violeta2||Violeta3||Violeta4||Violeta5){
                    int posicionElim2=Random.Range(1,5);
                    while(arrayInts2[posicionElim2]!=1){
                        posicionElim2=Random.Range(1,5);
                    }
                    if(posicionElim2==1){
                        arrayInts2[1]=0;
                        EmptyVioleta(1);
                        violeta1.gameObject.SetActive(true);
                    }  
                    if(posicionElim2==2){
                        arrayInts2[2]=0;
                        EmptyVioleta(1);
                        violeta2.gameObject.SetActive(true);
                    }  
                    if(posicionElim2==3){
                        arrayInts2[3]=0;
                        EmptyVioleta(1);
                        violeta3.gameObject.SetActive(true);
                    }  
                    if(posicionElim2==4){
                        arrayInts2[4]=0;
                        EmptyVioleta(1);
                        violeta4.gameObject.SetActive(true);
                    }  
                    if(posicionElim2==5){
                        arrayInts2[5]=0;
                        EmptyVioleta(1);
                        violeta5.gameObject.SetActive(true);
                    }  
                }      


                if(Azul1||Azul2||Azul3||Azul4||Azul5){
                    int posicionElim3=Random.Range(1,5);

                    while(arrayInts[posicionElim3]!=1){
                        posicionElim3=Random.Range(1,5);
                    }

                    
                    if(posicionElim3==1){
                        arrayInts[1]=0;
                        EmptyAzul(1);
                        azul1.gameObject.SetActive(true);
                    }  
                    if(posicionElim3==2){
                        arrayInts[2]=0;
                        EmptyAzul(1);
                        azul2.gameObject.SetActive(true);
                    }  
                    if(posicionElim3==3){
                        arrayInts[3]=0;
                        EmptyAzul(1);
                        azul3.gameObject.SetActive(true);
                    }  
                    if(posicionElim3==4){
                        arrayInts[4]=0;
                        EmptyAzul(1);
                        azul4.gameObject.SetActive(true);
                    }  
                    if(posicionElim3==5){
                        arrayInts[5]=0;
                        EmptyAzul(1);
                        azul5.gameObject.SetActive(true);
                    }  
                }      */
            
        
       
        if(col.gameObject.tag=="dorado"){
            completo+=1;
            Destroy(col.gameObject);      
        }
        if(col.gameObject.tag=="rojo"){
            completo+=1;
            Destroy(col.gameObject);      
        }
        if(col.gameObject.tag=="lila"){
            completo+=1;
            Destroy(col.gameObject);      
        }
        
		if(col.gameObject.tag=="Flower"){

            muerte-=2;
            Debug.Log("muerte"+muerte);
			HealthBack(2);
            Debug.Log("health"+currentHealth);
            Destroy(col.gameObject);     
        }


		if(col.gameObject.tag=="gemaAzul1"){
            Az+=1;
            //arrayInts[1]=1;
            //Azul1=true;
			FillAzul(2);
            col.gameObject.SetActive(false); 
            
        }
        if(col.gameObject.tag=="gemaAzul2"){
            Az+=1;
            //arrayInts[2]=1;
            //Azul2=true;
			FillAzul(2);
            col.gameObject.SetActive(false);    
            
        }
        if(col.gameObject.tag=="gemaAzul3"){
            Az+=1;
            //arrayInts[3]=1;
            //Azul3=true;
			FillAzul(2);
            col.gameObject.SetActive(false);      
        }
        if(col.gameObject.tag=="gemaAzul4"){
            Az+=1;
            //arrayInts[4]=1;
            //Azul4=true;
			FillAzul(2);
            col.gameObject.SetActive(false);    
        }
        if(col.gameObject.tag=="gemaAzul5"){
            Az+=1;
            //arrayInts[5]=1;
            //Azul5=true;
			FillAzul(2);
            col.gameObject.SetActive(false);     
        }


		if(col.gameObject.tag=="gemaVerde1"){
            Ver+=1;
            //arrayInts1[1]=1;
            //Verde1=true;
			FillVerde(2);
            col.gameObject.SetActive(false);   
        }
        if(col.gameObject.tag=="gemaVerde2"){
            Ver+=1;
            //arrayInts1[2]=1;
            //Verde2=true;
			FillVerde(2);
            col.gameObject.SetActive(false);    
        }
        if(col.gameObject.tag=="gemaVerde3"){
            Ver+=1;
            //arrayInts1[3]=1;
            //Verde3=true;
			FillVerde(2);
            col.gameObject.SetActive(false);     
        }
         if(col.gameObject.tag=="gemaVerde4"){
            Ver+=1;
            //arrayInts1[4]=1;
            //Verde4=true;
			FillVerde(2);
            col.gameObject.SetActive(false);     
        }
        if(col.gameObject.tag=="gemaVerde5"){
            Ver+=1;
            //arrayInts1[5]=1;
            //Verde5=true;
			FillVerde(2);
            col.gameObject.SetActive(false);  
        }


		if(col.gameObject.tag=="gemaVioleta1"){
            Vio++;
            //arrayInts2[1]=1;
            //Violeta1=true;
			FillVioleta(2);
            col.gameObject.SetActive(false);     
        }
        if(col.gameObject.tag=="gemaVioleta2"){
            Vio++;
            //arrayInts2[2]=1;
            //Violeta2=true;
			FillVioleta(2);
            col.gameObject.SetActive(false);      
        }
        if(col.gameObject.tag=="gemaVioleta3"){
            Vio++;
            //arrayInts2[3]=1;
            //Violeta3=true;
			FillVioleta(2);
            col.gameObject.SetActive(false);     
        }
        if(col.gameObject.tag=="gemaVioleta4"){
            Vio++;
            //arrayInts2[4]=1;
            //Violeta4=true;
			FillVioleta(2);
            col.gameObject.SetActive(false);     
        }
        if(col.gameObject.tag=="gemaVioleta5"){
            Vio++;
            //arrayInts2[5]=1;
            //Violeta5=true;
			FillVioleta(2);
            col.gameObject.SetActive(false);      
        }
        if(col.gameObject.tag=="puerta"){
            puerta=true;   
        }
    }
	
  void FixedUpdate(){
      Debug.Log("vio"+Vio);
	  float movx;
	  movx=Input.GetAxis("Horizontal");
	  anim.SetFloat("absmovx", Mathf.Abs(movx)) ;
	  enpiso=Physics2D.OverlapCircle(refpie.position,radio,1<<9);

	  anim.SetBool("enpiso",enpiso);
	  rb2D.velocity=new Vector2(velX * movx,rb2D.velocity.y);
	  if(Input.GetKey("d")||Input.GetKey("right")){
		rb2D.velocity=new Vector2(runSpeed, rb2D.velocity.y);
		
	  }
	  else if(Input.GetKey("a")||Input.GetKey("left")){
		rb2D.velocity=new Vector2(-runSpeed, rb2D.velocity.y);

	  }
	  else{
		rb2D.velocity=new Vector2(0, rb2D.velocity.y);
	  }
	  if(Input.GetKey(KeyCode.Space)&&enpiso){
		Instantiate(jumpAudio);
		rb2D.velocity=new Vector2(rb2D.velocity.x, jumpSpeed);
		
	  }
	  if(movx<0)transform.localScale=new Vector3(-1,1,1);
	  if(movx>0)transform.localScale=new Vector3(1,1,1);
      
      if(Az==5){
        panelr.gameObject.SetActive(true);
        StartCoroutine("wait3");
        //panelr.gameObject.SetActive(false);
        ROJO.gameObject.SetActive(true);
        compleAz=true;
        Az=10;

      };
      if(Ver==5){
        paneld.gameObject.SetActive(true);
        StartCoroutine("wait2");
        DORADO.gameObject.SetActive(true);
        compleVer=true;
        Ver=10;
      };
      if(Vio==5){
        panell.gameObject.SetActive(true);
        StartCoroutine("wait");
        //panell.gameObject.SetActive(false);
        LILA.gameObject.SetActive(true);
        compleVio=true;
        Vio=10;
      }
      //
        if(completo==3 && puerta ){
            Time.timeScale=0;  
            panelGana.gameObject.SetActive(true);
            PlayerPrefs.SetString("mejorPunt",PlayerPrefs.GetString("tiempo"));
        }
	}
    IEnumerator wait(){
        yield return new WaitForSeconds(10f);
        //panell.gameObject.SetActive(false);
        Destroy(panell.gameObject);
    }
    IEnumerator wait2(){
        yield return new WaitForSeconds(10f);
        //paneld.gameObject.SetActive(false);
        Destroy(paneld.gameObject);
    }
    IEnumerator wait3(){
        yield return new WaitForSeconds(10f);
        //panell.gameObject.SetActive(false);
        Destroy(panelr.gameObject);
    }
    public override bool Equals(object obj)
    {
        return obj is PlayerControl control &&
               EqualityComparer<Rigidbody2D>.Default.Equals(rb2D, control.rb2D);
    }
	void TakeDamage(int damage)
    {
        currentHealth -= damage;
        currentHealth = Mathf.Clamp(currentHealth, 0, maxHealth);
        playerStatusUI.SetHealth(healthRange);
    }
	void HealthBack(int health)
    {
        currentHealth += health;
        currentHealth = Mathf.Clamp(currentHealth, 0, maxHealth);
        playerStatusUI.SetHealth(healthRange);
    }
	void FillAzul(int damage)
    {
        currentBlue -= damage;
        currentBlue = Mathf.Clamp(currentBlue, 0, maxBlue);
        blueStatusUI.SetHealth(blueRange);
    }
    void EmptyAzul(int damage)
    {
        currentBlue += damage;
        currentBlue = Mathf.Clamp(currentBlue, 0, maxBlue);
        blueStatusUI.SetHealth(blueRange);
    }
	void FillVerde(int damage)
    {
        currentGreen -= damage;
        currentGreen = Mathf.Clamp(currentGreen, 0, maxGreen);
        GreenStatusUI.SetHealth(greenRange);
    }
    void EmptyVerde(int damage)
    {
        currentGreen += damage;
        currentGreen = Mathf.Clamp(currentGreen, 0, maxGreen);
        GreenStatusUI.SetHealth(greenRange);
    }
	void FillVioleta(int damage)
    {
        currentViolet -= damage;
        currentViolet = Mathf.Clamp(currentViolet, 0, maxViolet);
        VioletStatusUI.SetHealth(violetRange);
        /*if(currentBlue==maxBlue){
            Vio=true;
        }*/
    }
    void EmptyVioleta(int damage)
    {
        currentViolet += damage;
        currentViolet = Mathf.Clamp(currentViolet, 0, maxViolet);
        VioletStatusUI.SetHealth(violetRange);
    }
}